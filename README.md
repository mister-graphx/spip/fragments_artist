# Artist

Apporte a Fragment les composants (compositions, noisettes, dependances) necessaires,
a un site de portfolio destiné a un illustrateur, sculpteur, etc.

## Pages et compositions

* Compo Rubrique et article Book
* Compo Document Book
* Compo Rubrique Article Portfolio
* Compo Rubrique Documents Portfolio
* Compo Rubrique Filter box tag articles
* page Book
* Page medias


## Plugins/modules additionnels
[Le plugin licence](https://contrib.spip.net/Une-licence-pour-un-article)

Propose la gestion de liscences sur des articles, mais pas sur des documents apparement


[Le plugin Album](https://contrib.spip.net/Albums-3)

Permet une gestion par albums des documents.
Les "Albums" sont autonome ou associables aux objets editoriaux.

- pas de possibiliter de classer les documents dans les albums, hormis par le num titre

[Le plugin OrDoc](https://contrib.spip.net/Ordoc-ordonner-les-documents-attaches-4860)

Permet d'ordoner/classer les documents associés a un objet éditorial
En cours d'intégration avec Albums


Logos automatiques
Permet de renvoyer un document attaché si le LOGO_OBJET n'existe pas
http://spip-zone.rezo.narkive.com/j1kC4pSx/spip-zone-cherchage-de-logos-logo-auto-et-roles-de-documents
https://zone.spip.org/trac/spip-zone/browser/_plugins_/logo_auto/branches/logo_auto_php

Roles de documents
Permet de spécifier un role a un document attaché  un objet (logo, cover)

## CHANGELOGS

v1.0.1:
- ajout de la compo rubrique/article-book
- modèle rubrique_card et article_thumbnail
